#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <EEPROM.h>
#include <SocketIoClient.h>
#include <SPI.h>
#include <MFRC522.h>

#define USE_SERIAL Serial
#define EEPROM_STORAGE_VER 4
#define EEPROM_LENGTH 512

#define RST_PIN         0           // Configurable, see typical pin layout above
#define SS_PIN          15          // Configurable, see typical pin layout above


int stationId = 0;
int dockingId = 0;
String emitMessage;
boolean debugOn = true;
boolean eepromVerOk = false;

// constants won't change. Used here to set a pin number :
const int ledPin =  16;
const int debugLedPin =  2;
int debugLedState = LOW;
unsigned long previousMillis = 0;
const long interval = 100;    

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

// EEPROM start reading from the first byte (address 0) of the EEPROM
int address = 0;
byte value;

ESP8266WiFiMulti WiFiMulti;
SocketIoClient webSocket;
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
MFRC522::MIFARE_Key key;

void unlockEventHandler(const char* jsonStr, size_t length) {
  USE_SERIAL.printf("got message: %s\n", jsonStr);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(jsonStr);

   if (!root.success())
   {
      if(debugOn)
      {
         USE_SERIAL.println("parseObject() failed");
      }
      return;
   }

   int unLockStationId = atoi(root["stationId"]);
   int unLockDockingId = atoi(root["dockingId"]);
 
   if(debugOn)
   {
      USE_SERIAL.println("parse JSON unlock OK");
      USE_SERIAL.println(unLockStationId);
      USE_SERIAL.println(unLockDockingId);
   }
   
   if( (unLockStationId == stationId) && (unLockDockingId == dockingId))
   {
     // set the LED OFF for unlock
      if(debugOn)
      {
        USE_SERIAL.println("Unlocking ..");
      }
      
      digitalWrite(ledPin, HIGH);
      
      String str1 = "{\"stationId\":" ;
      String str2 = ",\"dockingId\":";
      String str3 = "}";   
      emitMessage = str1 + stationId + str2 + dockingId + str3;
      webSocket.emit("response:unlockDone",emitMessage.c_str());
   }
   
}

void lockEventHandler(const char* jsonStr, size_t length) {
  USE_SERIAL.printf("got message: %s\n", jsonStr);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(jsonStr);

   if (!root.success())
   {
      if(debugOn)
      {
         USE_SERIAL.println("parseObject() failed");
      }
      return;
   }

   int lockStationId = atoi(root["stationId"]);
   int lockDockingId = atoi(root["dockingId"]);
 
   if(debugOn)
   {
      USE_SERIAL.println("parse JSON lock OK");
      USE_SERIAL.println(lockStationId);
      USE_SERIAL.println(lockDockingId);
   }
   
   if( (lockStationId == stationId) && (lockDockingId == dockingId))
   {
     // set the LED OFF for unlock
      if(debugOn)
      {
        USE_SERIAL.println("Locking ..");
      }
      
      digitalWrite(ledPin, LOW);

      String str1 = "{\"stationId\":" ;
      String str2 = ",\"dockingId\":";
      String str3 = "}";   
      emitMessage = str1 + stationId + str2 + dockingId + str3;
      webSocket.emit("response:lockDone",emitMessage.c_str());
   }
   
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        USE_SERIAL.print(buffer[i] < 0x10 ? " 0" : " ");
        USE_SERIAL.print(buffer[i], HEX);
    }
}

void setup() {

    const char* ssid = "";
    const char* password = "";
    const char* host = "";
    int port = 0;
        
    // set the digital pin as output:
    pinMode(ledPin, OUTPUT);
    pinMode(debugLedPin, OUTPUT);
    
    digitalWrite(ledPin, LOW);
    digitalWrite(debugLedPin, LOW);

    //prepare ee prom
    EEPROM.begin(EEPROM_LENGTH);

    //prepare serial
    USE_SERIAL.begin(115200);
    while (!USE_SERIAL) {
         ; // wait for serial port to connect. Needed for native USB port only
    }
    
    if(debugOn)
    {
       //USE_SERIAL.setDebugOutput(true);
       USE_SERIAL.println();
       USE_SERIAL.println();
       USE_SERIAL.println();
    }

    
  
    for(uint8_t t = 4; t > 0; t--) {
        if(debugOn)
        {
          USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
          USE_SERIAL.flush();
        }
        delay(1000);
    }


    //prepare rfid
    SPI.begin();        // Init SPI bus

    delay(1000);
    mfrc522.PCD_Init(); // Init MFRC522 card

    delay(1000);
    
    mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
    USE_SERIAL.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));


    //read EEPROM here, the first byte is the storage version
    value = EEPROM.read(address);
    USE_SERIAL.println(value,DEC);
    if(value == EEPROM_STORAGE_VER)
    {
       String eePromStr = readEEPROM();
       StaticJsonBuffer<512> jsonBuffer;
       JsonObject& root = jsonBuffer.parseObject(eePromStr.c_str());
       USE_SERIAL.println("EEPROM read:" + eePromStr);
       if (!root.success())
       {
         if(debugOn)
         {
             USE_SERIAL.println("Malformed EEPROM data");
         }
         return;
       }
       
       ssid = root["ssid"];
       password = root["password"];
       stationId = root["stationId"];
       dockingId = root["dockingId"];
       host = root["host"];
       port = root["port"];
       eepromVerOk = true;
       //return;
    }
    else
    {
       USE_SERIAL.println("Please download EEPROM data\n");
       return;
    }
        
    WiFiMulti.addAP(ssid, password);

    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(100);
    }

    webSocket.on("command:unlock", unlockEventHandler);
    webSocket.on("command:lock", lockEventHandler);
    
    webSocket.begin(host,port,DEFAULT_URL);

    delay(1000);
    
    String str1 = "{\"text\": \"Station Id ";
    String str2 = " and Dock Id ";
    String str3 = " has joined\"}";   
   
    emitMessage = str1 + stationId + str2 + dockingId + str3;
    webSocket.emit("send:message",emitMessage.c_str());
   
}

void loop() {
    
    if(eepromVerOk){
       webSocket.loop();
    }

    serialEvent();
       
    if (stringComplete) {
      clearEEPROM();
      writeToEEPROM(inputString);
      // clear the string:
      inputString = "";
      stringComplete = false;
    }

    blinkDebugLed();

    //rfid stuff
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;

    // Show some details of the PICC (that is: the tag/card)
    mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
}

void serialEvent() {
  
  while (USE_SERIAL.available()) {
    // get the new byte:
    char inChar = (char)USE_SERIAL.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

String readEEPROM()
{
  String readStr = "";
  for (int i = 1 ; i < EEPROM_LENGTH ; i++) {
    byte readByte = EEPROM.read(i);
    if(readByte == 0)
    {
      break;
    }
    else
    {
       char readChar = (char) readByte;
       readStr += readChar;
    }
  }

  return readStr;
}

void writeToEEPROM(String eepromJSonString)
{
    USE_SERIAL.println("Write To EePROM" + eepromJSonString);
    EEPROM.write(0,EEPROM_STORAGE_VER);
    for (int i = 0 ; i < eepromJSonString.length() ; i++) {
       EEPROM.write(i + 1, eepromJSonString[i]);
    }
    EEPROM.commit();
}

void clearEEPROM()
{
  for (int i = 0 ; i < EEPROM_LENGTH ; i++) {
    EEPROM.write(i, 0);
  }

  EEPROM.commit();
}

void blinkDebugLed()
{
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (debugLedState == LOW) {
      debugLedState = HIGH;
    } else {
      debugLedState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(debugLedPin, debugLedState);
  }
}

